class Product
  include Mongoid::Document

  belongs_to :user
  
  field :sku, type: String
  field :name, type:String
  field :description, type: String
  field :quantity, type: Float
  field :price, type: Float

  validates :sku, :name, :description, :quantity, :price, :user_id, presence: true
  
end