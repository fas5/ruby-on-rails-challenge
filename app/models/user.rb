class User
  include Mongoid::Document
  include ActiveModel::SecurePassword
  
  has_many :products
  
  field :username, type: String
  field :email, type: String
  field :password_digest, type: String

  before_save { self.email = email.downcase }
  
  validates :username, 
    presence: true, 
    uniqueness: { case_sensitive: true }, 
    length: { minimum: 3, maximum: 15 }

  VALID_EMAIL_REGEX = /\A[\w+\-.]+@[a-z\d\-.]+\.[a-z]+\z/i
  validates :email,
    presence: true, 
    uniqueness: { case_sensitive: true }, 
    length: { maximum: 150 },
    format: { with: VALID_EMAIL_REGEX }

  has_secure_password

end