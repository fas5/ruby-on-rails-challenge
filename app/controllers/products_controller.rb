class ProductsController < ApplicationController

  before_action :set_product, only: [:edit, :update, :show, :destroy]

  def index
    @products = Product.page(params[:page]).per(5)
  end

  def new
    @product = Product.new
  end

  def edit
  end

  def create
    @product = Product.new(product_params)
    # TODO User login
    @product.user = User.first

    if @product.save
      flash[:success] = "Product successfully created!"
      redirect_to product_path(@product)
    else
      render 'new'
    end
  end

  def update
    if @product.update(product_params)
      flash[:success] = "Product successfully updated!"
      redirect_to product_path(@product)
    else
      render 'edit'
    end 
  end

  def show
  end

  def destroy
    @product.destroy
    flash[:danger] = "Product successfully deleted!"
    redirect_to products_path
  end

  private
    def set_product
      @product = Product.find(params[:id])
    end

    def product_params
      params.require(:product).permit(:sku, :name, :description, :quantity, :price)
    end

end